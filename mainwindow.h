#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QRect>
#include <QAction>
#include "graphwidget.h"
#include "node.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_sb_col_valueChanged(int arg1);
    void on_sb_row_valueChanged(int arg1);
    void on_pushButton_clicked();
    void menuAction_triggered(QAction* a);
    void on_nodeMenu(int id);
    void on_pb_select_clicked();
    void on_pb_save_clicked();
    void on_pb_load_clicked();
    void onJson(QByteArray s);

private:
    Ui::MainWindow *ui;
    GraphWidget *m_field;
    bool m_selecting;

};
#endif // MAINWINDOW_H
