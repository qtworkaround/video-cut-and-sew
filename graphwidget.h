#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include <QGraphicsView>
#include <QDebug>
#include <QQueue>
#include <QLabel>

class Node;

class GraphWidget : public QGraphicsView
{
    Q_OBJECT

    struct interest{
        QPoint topleft;
        quint32 zOrder;
        QString sname;
    };

public:
    GraphWidget(QWidget *parent = nullptr);
    quint32 node_cou;
    bool isckicked;
        bool scrolling;
    QQueue <Node *> allnodes;
    qreal scale_glob;

public slots:

    void zoomIn();
    void zoomOut();
    void addNode(qint32 x, qint32 y, quint32 id, QString fname, QString camname, qint32 z = 100);
    void delNode(quint32 id);
    void ClearAll();
    Node* GetNodeP(quint32 id);
    void setSize(QRect siz);
    void setLastUp();

signals:
    void nodeMenu(int ID);
    void nodeSel(int ID);
    void Showmenu(bool ok);
    void tooltipz(int f, QString str);
    void jsonStr(QByteArray s);
protected:

#ifndef QT_NO_WHEELEVENT
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
#endif
    void drawBackground(QPainter *painter, const QRectF &rect) Q_DECL_OVERRIDE;
    void scaleView(qreal scaleFactor);

private slots:
    void onNodeMenu(int id);
    void onRubberBandChanged(QRect rubberBandRect, QPointF fromScenePoint, QPointF toScenePoint);
    void CalculateThisArea(int x, int y, int x2, int y2);
    QList<Node*> GetReferenceList(QRect rect);
    bool DoTheyOverlap(QRect r1, QRect RectB);
    int compare(void *a, void *b);

private:
    int timerId;
    bool isphisic;
    QRect m_interestRect;

};

#endif // GRAPHWIDGET_H
