#ifndef NODE_H
#define NODE_H

#include <QGraphicsItem>
#include <QList>
#include <QMenu>
#include <QObject>
#include <QtGui>
#include <QFont>
#include <QHelpEvent>

class Edge;
class GraphWidget;
QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE

class Node : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    Node(GraphWidget *graphWidget = nullptr, QString file = "");

    enum { Type = UserType + 1 };
    int type() const Q_DECL_OVERRIDE { return Type; }

    QString camName;
    QString filepath;
    quint32 IDs;
    QRect sizze;

    void setSize(QRect sz);
    void changeZvalue(bool up);

    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;

signals:
    void ShowMenu(int ID);
    void ClearSel();

private slots:
    void makeHover();

protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void hoverEnterEvent ( QGraphicsSceneHoverEvent * event );
    void hoverLeaveEvent( QGraphicsSceneHoverEvent * event );
private:
    QPointF newPos;
    GraphWidget *graph;
    QString str;
    bool hovered;
    bool isChecked;
    QPixmap m_imst;

};

#endif // NODE_H
