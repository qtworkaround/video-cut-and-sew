#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMenu>
#include <QScrollBar>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_field = ui->graphicsView;
    connect(ui->graphicsView,SIGNAL(nodeMenu(int)),this,SLOT(on_nodeMenu(int)));
    connect(ui->graphicsView,SIGNAL(jsonStr(QByteArray)),this,SLOT(onJson(QByteArray)));

    m_selecting = false;

    /* Global ToDo's:
     * - Rect coordinates checks and fix(no minus widht's etc.)
     * - Mixer counter and Json output accomulation
     * - Lens correction for frames
     * - Multi file input
     * - Row's and Col's saving/loading along with frames
     */
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_sb_col_valueChanged(int arg1)
{
    m_field->setSize(QRect(0,0,1920*arg1,1080*ui->sb_row->value()));
}

void MainWindow::on_sb_row_valueChanged(int arg1)
{
    m_field->setSize(QRect(0,0,1920*ui->sb_col->value(),1080*arg1));
}

void MainWindow::on_pushButton_clicked()
{
    auto filename = QFileDialog::getOpenFileName();
    QString camname = ui->le_camname->text() + QString::number(ui->sb_cam->value()); ui->sb_cam->stepUp();
    m_field->addNode(0,0,m_field->node_cou,filename,camname);
    m_field->setLastUp();
}

void MainWindow::menuAction_triggered(QAction *a)
{
    Node* nd = m_field->GetNodeP(a->data().toUInt());
    if (a->text() == "Go forward")
        nd->changeZvalue(true);
    if (a->text() == "Go backward")
        nd->changeZvalue(false);
    if (a->text() == "Go to 0,0")
        nd->setPos(0,0);

}

void MainWindow::on_nodeMenu(int id)
{
    QMenu *menu = new QMenu(this);
    Node* nd = m_field->GetNodeP(id);
    QAction *act1 = new QAction; act1->setText(nd->camName); act1->setEnabled(false); menu->addAction(act1);
    QString spos = QString("x = %1 , y = %2 , z = %3").arg(QString::number(nd->x())).arg(QString::number(nd->y())).arg(nd->zValue());
    QAction *act2 = new QAction; act2->setText(spos); act2->setEnabled(false); menu->addAction(act2);
    menu->addSeparator();
    QAction *actg = new QAction; actg->setData(id); actg->setText("Go to 0,0"); menu->addAction(actg);
    menu->addSeparator();
    QAction *actf = new QAction; actf->setData(id); actf->setText("Go forward"); menu->addAction(actf);
    QAction *actb = new QAction; actb->setData(id); actb->setText("Go backward"); menu->addAction(actb);

    connect(menu,SIGNAL( triggered(QAction*)),this,SLOT(menuAction_triggered(QAction*)));
    menu->exec(QCursor::pos());
}

void MainWindow::on_pb_select_clicked()
{
    m_selecting = true;
}

void MainWindow::on_pb_save_clicked()
{
    // to do: make saving of nodes(names, ids, paths, positions) to qsettings
    QSettings ini("saves/lastscene.ini",QSettings::IniFormat); ini.clear();
    ini.setValue("camCount",m_field->allnodes.count());
    for (int var = 0; var < m_field->allnodes.count(); var++) {
        ini.setValue("cam_"+QString::number(var)+"/filePath",m_field->allnodes.at(var)->filepath);
        ini.setValue("cam_"+QString::number(var)+"/camName",m_field->allnodes.at(var)->camName);
        ini.setValue("cam_"+QString::number(var)+"/id",m_field->allnodes.at(var)->IDs);
        ini.setValue("cam_"+QString::number(var)+"/x",qint32(m_field->allnodes.at(var)->x()));
        ini.setValue("cam_"+QString::number(var)+"/y",qint32(m_field->allnodes.at(var)->y()));
        ini.setValue("cam_"+QString::number(var)+"/z",qint32(m_field->allnodes.at(var)->zValue()));
    }
    ini.sync();
}

void MainWindow::on_pb_load_clicked()
{
    // to do: make loading nodes from qsettings (names, ids, paths, positions)
    QSettings ini("saves/lastscene.ini",QSettings::IniFormat);
    int cou = ini.value("camCount").toInt();
    m_field->ClearAll();
    for (int var = 0; var < cou; var++) {
        QString filePath = ini.value("cam_"+QString::number(var)+"/filePath").toString();
        QString camName = ini.value("cam_"+QString::number(var)+"/camName").toString();
        quint32 id = ini.value("cam_"+QString::number(var)+"/id").toUInt();
        qint32 x = ini.value("cam_"+QString::number(var)+"/x").toInt();
        qint32 y = ini.value("cam_"+QString::number(var)+"/y").toInt();
        qint32 z = ini.value("cam_"+QString::number(var)+"/z").toInt();
        m_field->addNode(x,y,id,filePath,camName,z);
        if (m_field->node_cou < m_field->allnodes.at(var)->IDs)
            m_field->node_cou = m_field->allnodes.at(var)->IDs + 1;
    }
}

void MainWindow::onJson(QByteArray s)
{
    ui->textEdit->setText(QString::fromStdString(s.toStdString()));
}
