#include "graphwidget.h"
//#include "edge.h"
#include "node.h"
#include <math.h>
#include <QKeyEvent>
#include <QGraphicsItem>

GraphWidget::GraphWidget(QWidget *parent)
    : QGraphicsView(parent), timerId(0)
{
    QGraphicsScene *scene = new QGraphicsScene(this);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene->setSceneRect(0, 0, 3840, 2160);
    setScene(scene);
    node_cou = 0;
    setCacheMode(CacheBackground);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    setDragMode(QGraphicsView::RubberBandDrag);
    scale(qreal(0.2), qreal(0.2));
    setMinimumSize(400, 400);
    isphisic = true;
    scrolling = false;
    connect(this,SIGNAL(rubberBandChanged(QRect,QPointF,QPointF)),this,SLOT(onRubberBandChanged(QRect,QPointF,QPointF)));
}


#ifndef QT_NO_WHEELEVENT
void GraphWidget::wheelEvent(QWheelEvent *event)
{
    scaleView(pow(double(1.2), event->delta() / 240.0));
}
#endif

void GraphWidget::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);

    // Shadow
    QRectF sceneRect = this->sceneRect();

    QRadialGradient gradient(sceneRect.center(),800);
    gradient.setColorAt(0, Qt::white);
    gradient.setColorAt(1, QColor(Qt::lightGray).light(120));
    painter->fillRect(rect.intersected(sceneRect), gradient);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(sceneRect);

}

void GraphWidget::scaleView(qreal scaleFactor)
{
    qreal factor = transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.02 || factor > 100)
        return;
    scale(scaleFactor, scaleFactor);
}

void GraphWidget::ClearAll()
{
    QList<Node *> nodes;
    foreach (QGraphicsItem *item, scene()->items()) {
        if (Node *node = qgraphicsitem_cast<Node *>(item))
            nodes << node;
    }
    foreach (Node *node, nodes)
    delete node;
    node_cou = 0;
    allnodes.clear();
}

Node* GraphWidget::GetNodeP(quint32 id)
{
    for (int var = 0; var < allnodes.count(); var++) {
        if (allnodes.at(var)->IDs == id) {
            return allnodes.at(var);
        }
    }
    return nullptr;
}

void GraphWidget::zoomIn()
{
    scaleView(qreal(1.1));
}

void GraphWidget::zoomOut()
{
    scaleView(1 / qreal(1.1));
}

void GraphWidget::addNode(qint32 x, qint32 y, quint32 id, QString fname, QString camname, qint32 z)
{
    if (allnodes.count() < 250) {
        Node *node1 = new Node(this,fname);
        node1->IDs = id;
        node1->camName = camname;
        node1->setX(x);
        node1->setY(y);
        node1->setZValue(z);
        connect(node1,SIGNAL(ShowMenu(int)),this,SLOT(onNodeMenu(int)));
        scene()->addItem(node1);
        allnodes.enqueue(node1);
        qDebug() << "creating node!";
        node1->setPos(x, y);
        node_cou++;
    }
}

void GraphWidget::delNode(quint32 id)
{
    Node * ss = GetNodeP(id);
    scene()->removeItem(ss);
    allnodes.removeOne(ss);
    delete ss;
}

void GraphWidget::setSize(QRect siz)
{
    this->scene()->setSceneRect(siz);
}

void GraphWidget::setLastUp()
{
    qreal z = 100.0;
    foreach (Node* nd, allnodes) {
        if (nd->zValue() > z)
            z = nd->zValue();
    }
    allnodes.last()->setZValue(z);
}

void GraphWidget::onNodeMenu(int id)
{
    emit nodeMenu(id);
}

void GraphWidget::onRubberBandChanged(QRect rubberBandRect, QPointF fromScenePoint, QPointF toScenePoint)
{
    static qreal fromScenePointx = 0.0;
    static qreal fromScenePointy = 0.0;
    static qreal toScenePointx = 0.0;
    static qreal toScenePointy = 0.0;

    if (rubberBandRect.x() == 0 && rubberBandRect.width() == 0 && fromScenePoint.x() == 0 && toScenePoint.y() == 0) {
        // to do: check and reverse if not lu->rb selection
        CalculateThisArea(int(fromScenePointx),int(fromScenePointy),int(toScenePointx),int(toScenePointy));
        //qDebug() << "------------ new rect got --------------";
    }

    fromScenePointx = fromScenePoint.x();
    fromScenePointy = fromScenePoint.y();
    toScenePointx = toScenePoint.x();
    toScenePointy = toScenePoint.y();

}

int GraphWidget::compare(void * a, void * b)
{

  interest *orderA = static_cast<interest*>(a);
  interest *orderB = static_cast<interest*>(b);

  return int( orderB->zOrder - orderA->zOrder );
}

void GraphWidget::CalculateThisArea(int x, int y, int x2, int y2)
{
    QRect ourRect(QPoint(x,y),QPoint(x2,y2));
    int tail = ourRect.width() %16;
    if (tail != 0) {
       ourRect.setWidth(ourRect.width()+(16-tail));
    }
    tail = ourRect.height()%16;
    if (tail != 0) {
       ourRect.setHeight(ourRect.height()+(16-tail));
    }
    QList<Node*> refNodes = GetReferenceList(ourRect);
    QList<interest> areas;
    Node* tnd = new Node();
    tnd->setSize(QRect(QPoint(0,0),QPoint(ourRect.width(),ourRect.height())));
    for (int var = 0; var < refNodes.count(); var++) {

        //qDebug() << ">> Here is calculated point for " << refNodes.at(var)->camName << " - " << tnd->mapFromItem(refNodes.at(var),ourRect.topLeft());
        QPointF p = refNodes.at(var)->pos() - ourRect.topLeft();
        interest i; i.topleft = p.toPoint(); i.zOrder = refNodes.at(var)->zValue();
        i.sname = refNodes.at(var)->camName;
        areas.append(i);
        //qDebug() << ">> Here is calculated point for " << refNodes.at(var)->camName << " - " << p.toPoint();
    }

    qSort(areas.begin(),areas.end(),[](const interest& a, const interest& b) { return a.zOrder < b.zOrder; });

    QJsonObject mixerObj;
    mixerObj.insert("id", QJsonValue::fromVariant("mixer_2"));
    mixerObj.insert("w", QJsonValue::fromVariant(ourRect.width()));
    mixerObj.insert("h", QJsonValue::fromVariant(ourRect.height()));

    QJsonArray sourcesObject;

    for (int var = 0; var < areas.count(); var++) {
        QJsonObject areaObject;
        areaObject.insert("source", QJsonValue::fromVariant(areas.at(var).sname));
        areaObject.insert("brightness", QJsonValue::fromVariant(128));
        areaObject.insert("contrast", QJsonValue::fromVariant(128));
        areaObject.insert("lens", QJsonValue::fromVariant(0));

        QJsonArray sceneObhect;

        QJsonObject coordTL;
        coordTL.insert("x", QJsonValue::fromVariant(areas.at(var).topleft.x()));
        coordTL.insert("y", QJsonValue::fromVariant(areas.at(var).topleft.y()));
        QJsonObject coordTR;
        coordTR.insert("x", QJsonValue::fromVariant(areas.at(var).topleft.x()+1920));
        coordTR.insert("y", QJsonValue::fromVariant(areas.at(var).topleft.y()));
        QJsonObject coordBR;
        coordBR.insert("x", QJsonValue::fromVariant(areas.at(var).topleft.x()+1920));
        coordBR.insert("y", QJsonValue::fromVariant(areas.at(var).topleft.y()+1080));
        QJsonObject coordBL;
        coordBL.insert("x", QJsonValue::fromVariant(areas.at(var).topleft.x()));
        coordBL.insert("y", QJsonValue::fromVariant(areas.at(var).topleft.y()+1080));

        sceneObhect.push_back(coordTL);
        sceneObhect.push_back(coordTR);
        sceneObhect.push_back(coordBR);
        sceneObhect.push_back(coordBL);

        areaObject.insert("scene",sceneObhect);
        sourcesObject.push_back(areaObject);
    }
    mixerObj.insert("sources",sourcesObject);

    QJsonDocument doc(mixerObj);
    QFile file("test.json");
    file.open(QIODevice::WriteOnly);
    file.write(doc.toJson());
    file.close();

    emit jsonStr(doc.toJson());

    //qDebug() << doc.toJson().;

}

QList<Node *> GraphWidget::GetReferenceList(QRect rect)
{
    QList<Node*> nodes;
    //qDebug() << "----- checking overlapping with items ----";
    foreach (QGraphicsItem* item, this->scene()->items()) {
        int w = item->boundingRect().toRect().width();
        int h = item->boundingRect().toRect().height();
        QRect itemrect = QRect(QPoint(int(item->x()),int(item->y())),QPoint(int(item->x()+w),int(item->y()+h)));
        if (DoTheyOverlap(rect, itemrect)) {
            //qDebug() << "--- true ---- item rect = " << itemrect << ", our rect = " << rect;
            nodes.append(static_cast<Node*>(item));
        } else {
            //qDebug() << "--- false -- item rect = " << itemrect << ", our rect = " << rect;
        }
    }
    return nodes;
}

bool GraphWidget::DoTheyOverlap(QRect RectA, QRect RectB)
{
    // lib option
    return RectA.intersects(RectB);

    // manual option
    int Ax2 = RectA.x() + RectA.width();
    int Bx2 = RectB.x() + RectB.width();
    int Ay2 = RectA.y() + RectA.height();
    int By2 = RectB.y() + RectB.height();
    return (RectA.x() < Bx2 && Ax2 > RectB.x() &&
        RectA.y() < By2 && Ay2 > RectB.y());
}
