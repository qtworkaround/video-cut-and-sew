//#include "edge.h"
#include "node.h"
#include "graphwidget.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOption>

Node::Node(GraphWidget *graphWidget, QString file)
    : graph(graphWidget)
{
    filepath = file;
    setFlag(ItemIsMovable);
    setFlag(ItemIsSelectable);
    sizze = QRect(0,0,1920,1080);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setZValue(100);
    hovered = false; isChecked = false;
    setAcceptHoverEvents(true);
    m_imst.load(filepath);
}

void Node::setSize(QRect sz)
{
    sizze = sz;
}

void Node::changeZvalue(bool up)
{
    qreal zz = zValue();
    if (up) {
        zz += 1.0;
    } else {
        zz -= 1.0;
    }
    setZValue(zz);
}

QRectF Node::boundingRect() const
{
    return QRectF(sizze.x(), sizze.y(), sizze.width(), sizze.height());
}

QPainterPath Node::shape() const
{
    QPainterPath path;
    path.addRect(sizze);
    return path;
}

void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    painter->drawPixmap(sizze,m_imst);
}

void Node::makeHover()
{
    hovered = true;
}

void Node::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    if (isChecked) {
        emit ShowMenu(IDs);
        isChecked = false;
    }
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    isChecked = true;
    if( event->modifiers() & Qt::CTRL ) // you know that
    {
        event->ignore(); // and that's all you need to know :)
    } else {
        QGraphicsItem::mousePressEvent(event);
    }
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
}

void Node::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    hovered = true;
}

void Node::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    hovered = false;
}
